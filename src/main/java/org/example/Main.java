package org.example;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Teacher.class)
                .buildSessionFactory();
        EntityManager entityManager = factory.createEntityManager();

        entityManager.getTransaction().begin();
        Student student = entityManager.find(Student.class, 1);
        entityManager.getTransaction().commit();
        System.out.println(student);
    }
}
